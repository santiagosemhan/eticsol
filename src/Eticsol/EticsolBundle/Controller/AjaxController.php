<?php

namespace Eticsol\EticsolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of AjaxController
 *
 * @author matias
 */
class AjaxController extends Controller {

    public function localidadesAction() {
        $provincia_id = $this->getRequest()->request->get('province_id');

        $em = $this->getDoctrine()->getManager();

        $provincia = $em->getRepository('EticsolBundle:Provincia')->findById($provincia_id);

        $localidades = $em->getRepository('EticsolBundle:Localidad')->findByProvincia($provincia);


        return $this->render('EticsolBundle:Default:localidades.html.twig', array(
                    'localidades' => $localidades
        ));
    }

    public function getPrecioProductoAction(Request $request) {

        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EticsolBundle:Producto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        return new JsonResponse($entity->getPrecio());
    }

}
