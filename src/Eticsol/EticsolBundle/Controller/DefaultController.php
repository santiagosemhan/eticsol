<?php

namespace Eticsol\EticsolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $alumnos = array('juan','pepe','santi','mati','etc');
        
        $provinciaRepository = $this->getDoctrine()->getRepository('EticsolBundle:Provincia');
        
        return $this->render('EticsolBundle:Default:index.html.twig',array('alumnos'=>$alumnos));
    }
    
    public function pruebaAction(Request $request)
    {
        
        $provinciaRepository = $this->getDoctrine()->getRepository('EticsolBundle:Provincia');
        
        $nombre = $request->get('nombre');
        $otroNombre = $request->get('otroNombre');
        
        var_dump($nombre);
        var_dump($otroNombre);
        
        exit;
        
        return $this->render('EticsolBundle:Default:prueba.html.twig');
    }
}
