<?php

namespace Eticsol\EticsolBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eticsol\EticsolBundle\Entity\Provincia;

/**
 * Description of ProvinciasLocalidades
 *
 * @author matias solis de la torre
 */
class ProvinciasData extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 1;
    }

    public function load(ObjectManager $manager) {
        $provincias = array(
            array('id' => 1, 'descripcion' => 'Buenos Aires'),
            array('id' => 2, 'descripcion' => 'Buenos Aires-GBA'),
            array('id' => 3, 'descripcion' => 'Capital Federal'),
            array('id' => 4, 'descripcion' => 'Catamarca'),
            array('id' => 5, 'descripcion' => 'Chaco'),
            array('id' => 6, 'descripcion' => 'Chubut'),
            array('id' => 7, 'descripcion' => 'Córdoba'),
            array('id' => 8, 'descripcion' => 'Corrientes'),
            array('id' => 9, 'descripcion' => 'Entre Ríos'),
            array('id' => 10, 'descripcion' => 'Formosa'),
            array('id' => 11, 'descripcion' => 'Jujuy'),
            array('id' => 12, 'descripcion' => 'La Pampa'),
            array('id' => 13, 'descripcion' => 'La Rioja'),
            array('id' => 14, 'descripcion' => 'Mendoza'),
            array('id' => 15, 'descripcion' => 'Misiones'),
            array('id' => 16, 'descripcion' => 'Neuquén'),
            array('id' => 17, 'descripcion' => 'Río Negro'),
            array('id' => 18, 'descripcion' => 'Salta'),
            array('id' => 19, 'descripcion' => 'San Juan'),
            array('id' => 20, 'descripcion' => 'San Luis'),
            array('id' => 21, 'descripcion' => 'Santa Cruz'),
            array('id' => 22, 'descripcion' => 'Santa Fe'),
            array('id' => 23, 'descripcion' => 'Santiago del Estero'),
            array('id' => 24, 'descripcion' => 'Tierra del Fuego'),
            array('id' => 25, 'descripcion' => 'Tucumán'),
        );

        foreach ($provincias as $provincia) {
            $entidadProv = new Provincia();

            $entidadProv->setDescripcion($provincia['descripcion']);

            $manager->persist($entidadProv);
            $this->addReference('provincia' . $provincia['id'], $entidadProv);
        }


        $manager->flush();
    }

}


