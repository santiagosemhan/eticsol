<?php

namespace Eticsol\EticsolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Detalle
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Detalle {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="detalle")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    private $producto;
    
    /**
     * @ORM\ManyToOne(targetEntity="Factura", inversedBy="detalle")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id")
     */
    private $factura;
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return Detalle
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad() {
        return $this->cantidad;
    }


    /**
     * Set producto
     *
     * @param \Eticsol\EticsolBundle\Entity\Producto $producto
     * @return Detalle
     */
    public function setProducto(\Eticsol\EticsolBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \Eticsol\EticsolBundle\Entity\Producto 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set factura
     *
     * @param \Eticsol\EticsolBundle\Entity\Factura $factura
     * @return Detalle
     */
    public function setFactura(\Eticsol\EticsolBundle\Entity\Factura $factura = null)
    {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \Eticsol\EticsolBundle\Entity\Factura 
     */
    public function getFactura()
    {
        return $this->factura;
    }
}
