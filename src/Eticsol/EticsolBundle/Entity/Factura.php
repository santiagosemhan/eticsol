<?php

namespace Eticsol\EticsolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Factura
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Factura {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroFactura", type="string", length=255)
     */
    private $numeroFactura;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal")
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="Localidad")
     * @ORM\JoinColumn(name="localidad_id", referencedColumnName="id")
     */
    private $localidad;

    /**
     * @ORM\ManyToOne(targetEntity="Iva")
     * @ORM\JoinColumn(name="iva_id", referencedColumnName="id")
     */
    private $iva;

    /**
     * @ORM\ManyToOne(targetEntity="CondicionPago")
     * @ORM\JoinColumn(name="condicion_pago_id", referencedColumnName="id")
     */
    private $condicionPago;
    
    /**
     * @ORM\OneToMany(targetEntity="Detalle", mappedBy="factura",cascade={"persist"})
     */
    private $detalle;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set numeroFactura
     *
     * @param string $numeroFactura
     * @return Factura
     */
    public function setNumeroFactura($numeroFactura) {
        $this->numeroFactura = $numeroFactura;

        return $this;
    }

    /**
     * Get numeroFactura
     *
     * @return string 
     */
    public function getNumeroFactura() {
        return $this->numeroFactura;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Factura
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return Factura
     */
    public function setTotal($total) {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal() {
        return $this->total;
    }

    /**
     * Set localidad
     *
     * @param \Eticsol\EticsolBundle\Entity\Localidad $localidad
     * @return Factura
     */
    public function setLocalidad(\Eticsol\EticsolBundle\Entity\Localidad $localidad = null) {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return \Eticsol\EticsolBundle\Entity\Localidad 
     */
    public function getLocalidad() {
        return $this->localidad;
    }

    /**
     * Set iva
     *
     * @param \Eticsol\EticsolBundle\Entity\Iva $iva
     * @return Factura
     */
    public function setIva(\Eticsol\EticsolBundle\Entity\Iva $iva = null) {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return \Eticsol\EticsolBundle\Entity\Iva 
     */
    public function getIva() {
        return $this->iva;
    }

    /**
     * Set condicionPago
     *
     * @param \Eticsol\EticsolBundle\Entity\CondicionPago $condicionPago
     * @return Factura
     */
    public function setCondicionPago(\Eticsol\EticsolBundle\Entity\CondicionPago $condicionPago = null) {
        $this->condicionPago = $condicionPago;

        return $this;
    }

    /**
     * Get condicionPago
     *
     * @return \Eticsol\EticsolBundle\Entity\CondicionPago 
     */
    public function getCondicionPago() {
        return $this->condicionPago;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->detalle = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add detalle
     *
     * @param \Eticsol\EticsolBundle\Entity\Detalle $detalle
     * @return Factura
     */
    public function addDetalle(\Eticsol\EticsolBundle\Entity\Detalle $detalle)
    {
        $this->detalle[] = $detalle;

        return $this;
    }

    /**
     * Remove detalle
     *
     * @param \Eticsol\EticsolBundle\Entity\Detalle $detalle
     */
    public function removeDetalle(\Eticsol\EticsolBundle\Entity\Detalle $detalle)
    {
        $this->detalle->removeElement($detalle);
    }

    /**
     * Get detalle
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }
}
