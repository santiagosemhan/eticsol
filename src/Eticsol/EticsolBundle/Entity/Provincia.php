<?php

namespace Eticsol\EticsolBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Provincia
 *
 * @ORM\Table(name="provincia" , uniqueConstraints={@ORM\UniqueConstraint(name="descripcion_idx", columns={"descripcion"})} )
 * 
 * @UniqueEntity("descripcion")
 * 
 * @ORM\Entity(repositoryClass="ProvinciaRepository")
 */
class Provincia {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * 
     * @Assert\NotNull( message = "Este campo no puede ir vacío." )
     * 
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     */
    private $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Localidad", mappedBy="provincia")
     */
    private $localidades;

    public function __toString() {
        return $this->descripcion;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->localidades = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Provincia
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Add localidades
     *
     * @param \Eticsol\EticsolBundle\Entity\Localidad $localidades
     * @return Provincia
     */
    public function addLocalidade(\Eticsol\EticsolBundle\Entity\Localidad $localidades) {
        $this->localidades[] = $localidades;

        return $this;
    }

    /**
     * Remove localidades
     *
     * @param \Eticsol\EticsolBundle\Entity\Localidad $localidades
     */
    public function removeLocalidade(\Eticsol\EticsolBundle\Entity\Localidad $localidades) {
        $this->localidades->removeElement($localidades);
    }

    /**
     * Get localidades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocalidades() {
        return $this->localidades;
    }

}
