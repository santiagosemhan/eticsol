<?php

namespace Eticsol\EticsolBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DetalleType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('cantidad', 'number', array(
                    'data' => 1,
                    'attr' => array(
                        'class' => 'cantidad',
                    ),
                ))
                ->add('producto', 'entity', array(
                    'attr' => array(
                        'class' => 'select_producto',
                    ),
                    'empty_value' => 'Seleccionar',
                    'class' => 'EticsolBundle:Producto',
                    'property' => 'descripcion',
                    'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                        ->where('p.activo = true');
            }
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Eticsol\EticsolBundle\Entity\Detalle'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'eticsol_eticsolbundle_detalle';
    }

}
