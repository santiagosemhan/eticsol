<?php

namespace Eticsol\EticsolBundle\Form;

use Doctrine\ORM\EntityRepository;
use Eticsol\EticsolBundle\Form\EventListener\AddLocalidadFieldSubscriber;
use Eticsol\EticsolBundle\Form\EventListener\AddProvinciaFieldSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FacturaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $factory = $builder->getFormFactory();
        $provinciaSubscriber = new AddProvinciaFieldSubscriber($factory);
        $builder->addEventSubscriber($provinciaSubscriber);
        $localidadSubscriber = new AddLocalidadFieldSubscriber($factory);
        $builder->addEventSubscriber($localidadSubscriber);

        $builder
                ->add('numeroFactura', 'text', array(
                    'attr' => array(
                        'placeholder' => '0000x'
                    )
                ))
                ->add('fecha', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'label' => 'Fecha',
                    'attr' => array('class' => 'date')))
                ->add('total', 'number', array(
                    'attr' => array('readOnly' => true),
                    'data' => 0,
                ))
//                ->add('localidad')
                ->add('iva', 'entity', array(
                    'empty_value' => 'Seleccionar',
                    'class' => 'EticsolBundle:Iva',
                    'property' => 'descripcion',
                    'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('i')
                        ->where('i.activo = true');
            }
                ))
                ->add('condicionPago', 'entity', array(
                    'empty_value' => 'Seleccionar',
                    'class' => 'EticsolBundle:CondicionPago',
                    'property' => 'descripcion',
                    'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('cp')
                        ->where('cp.activo = true');
            }
                ))
                ->add('detalle', 'collection', array(
                    'type' => new DetalleType(),
                    'by_reference' => false,
                    'allow_delete' => true,
                    'allow_add' => true,
                    'prototype' => true,
                    'attr' => array(
                        'class' => 'row detalles'
            )))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Eticsol\EticsolBundle\Entity\Factura'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'eticsol_eticsolbundle_factura';
    }

}
